<?php


namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    /**
     * @return @Route("/article")
     */
    public function homepage()
    {
        return new Response('New page');
    }

    /**
     * @param $slug
     * @return Response @Route("/article/{slug}")
     */
    public function show($slug)
    {
        $comments = [ 3, 2, 1];
        return $this->render('article/show.html.twig', [
            'title' => ucfirst(str_replace('-', ' ', $slug)),
            'comments' => $comments,
        ]);
    }
}
